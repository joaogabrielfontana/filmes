//
//  DetailedMoviePresenter.swift
//  Filmes
//
//  Created by João Gabriel on 15/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import Foundation
import Moya
import RxMoya
import RxSwift

class DetailedMoviePresenter {
    
    private var detailedMovieView: DetailedMovieView
    
    private let disposeBag = DisposeBag()
    
    private let repository: MainRepository
    
    init(repository: MainRepository, view: DetailedMovieView) {
        self.repository = repository
        self.detailedMovieView = view
    }
    
    func saveFavoriteMovie(movieId: Int) {
        self.repository.saveFavoriteMovie(movieId: movieId)
            .subscribe(onCompleted: { [weak self] in
                self?.detailedMovieView.saveFavoriteMovie()
                }, onError: { [weak self] _ in
                    self?.detailedMovieView.showSaveFavoriteError(error: "Não foi possível salvar o filme como favorito")
            }).disposed(by: self.disposeBag)
    }
    
    func removeFavoriteMovie(movieId: Int) {
        self.repository.removeFavoriteMovie(movieId: movieId)
            .subscribe(onCompleted: { [weak self] in
                self?.detailedMovieView.removeFavoriteMovie()
                }, onError: { [weak self] _ in
                    self?.detailedMovieView.showRemoveFavoriteError(error: "Não foi possível remover o filme dos favoritos")
            }).disposed(by: self.disposeBag)
    }
    
    //Internet then Cache
    func getDetailedMovie(movieID: Int) {
        Single.zip(self.repository.getDetailedMovie(movieID: movieID), self.repository.isFavoriteMovie(movieId: movieID), resultSelector: { (detailedMovie: DetailedMovie, isFavorite: Bool) in
            return (detailedMovie, isFavorite)
        }).subscribe(onSuccess: { [weak self] (detailedMovie, isFavorite) in
            self?.detailedMovieView.addDetailedMovie(detailedMovie: detailedMovie, isFavorite: isFavorite)
            }, onError: { [weak self] error in
                self?.detailedMovieView.showGetMovieError(error: error.localizedDescription)
        }).disposed(by: self.disposeBag)
    }
    
    
    //Cache then Internet
    func getDetailedMovieFromCache(movieID: Int)  {
        Single.zip(self.repository.getDetailedMovieFromCache(movieID: movieID), self.repository.isFavoriteMovie(movieId: movieID), resultSelector: { (detailedMovie: DetailedMovie, isFavorite: Bool) in
            return (detailedMovie, isFavorite)
        }).subscribe(onSuccess: { [weak self] (detailedMovie, isFavorite) in
            self?.detailedMovieView.addDetailedMovie(detailedMovie: detailedMovie, isFavorite: isFavorite)
            }, onError: { [weak self] (error) in
                self?.detailedMovieView.showGetMovieError(error: error.localizedDescription)
        }).disposed(by: self.disposeBag)
    }
}
