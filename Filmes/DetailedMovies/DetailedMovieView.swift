//
//  DetailedMovieView.swift
//  Filmes
//
//  Created by João Gabriel on 15/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import Foundation

protocol DetailedMovieView {
    func addDetailedMovie(detailedMovie: DetailedMovie, isFavorite: Bool)
    func showGetMovieError(error: String)
    func saveFavoriteMovie()
    func removeFavoriteMovie()
    func showSaveFavoriteError(error: String)
    func showRemoveFavoriteError(error: String)
}
