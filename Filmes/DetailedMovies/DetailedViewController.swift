//
//  DetailedViewController.swift
//  Filmes
//
//  Created by João Gabriel on 11/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import RxCocoa

class DetailedViewController: UIViewController, Storyboarded {
    
    //MARK: IBOutlets
    @IBOutlet weak var moviePosterImageView: UIImageView!
    
    @IBOutlet weak var movieTitleLabel: UILabel!
    
    @IBOutlet weak var movieDescriptionLabel: UILabel!
    
    @IBOutlet weak var movieRuntimeLabel: UILabel!
    
    @IBOutlet weak var movieBudgetLabel: UILabel!
    
    @IBOutlet weak var movieRevenueLabel: UILabel!
    
    @IBOutlet weak var movieVoteAverageLabel: UILabel!
    
    @IBOutlet weak var favouriteButton: UIButton! {
        didSet {
            self.favouriteButton.setBackgroundImage(UIImage(named: "outlined_star"), for: .normal)
        }
    }
    
    //MARK: Variables
    
    
    
    var presenter: DetailedMoviePresenter!
    
    var id: Int!
    
    let currencyFormatter = NumberFormatter()
    
    weak var coordinator: MainCoordinator?
    
    var isFavorite: Bool!
    
    private let disposableBag = DisposeBag()
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.currencyFormatter.numberStyle = .currency
        self.presenter.getDetailedMovieFromCache(movieID: self.id)
        self.setUpObservables()
    }
    
    private func setUpObservables() {
        self.favouriteButton.rx.tap.asObservable()
            .subscribe(onNext: { (_) in
                if self.isFavorite {
                    self.presenter.removeFavoriteMovie(movieId: self.id)
                } else {
                    self.presenter.saveFavoriteMovie(movieId: self.id)
                }
            }).disposed(by: disposableBag)
    }
    
    private func setUpView(detailedMovie: DetailedMovie, isFavorite: Bool) {
        self.movieDescriptionLabel.text = detailedMovie.overview
        self.movieRuntimeLabel.text = String(detailedMovie.runtime)
        self.movieBudgetLabel.text = self.currencyFormatter.string(from: NSNumber(value: detailedMovie.budget))
        self.movieRevenueLabel.text = self.currencyFormatter.string(from: NSNumber(value: detailedMovie.revenue))
        self.movieVoteAverageLabel.text = String(detailedMovie.voteAverage)
        self.navigationItem.title = detailedMovie.title
        self.movieTitleLabel.text = detailedMovie.title
        let placeholder = UIImage(named: "placeholder")
        self.moviePosterImageView.kf.indicatorType = .activity
        self.moviePosterImageView.kf.setImage(with: URL(string: detailedMovie.posterURL), placeholder: placeholder)
        if isFavorite {
            self.favouriteButton.setImage(UIImage(named: "filled_star"), for: .normal)
            self.isFavorite = true
        } else {
            self.favouriteButton.setImage(UIImage(named: "outlined_star"), for: .normal)
            self.isFavorite = false
        }
    }
}

//MVP
extension DetailedViewController: DetailedMovieView {
    func saveFavoriteMovie() {
        self.favouriteButton.setImage(UIImage(named: "filled_star"), for: .normal)
        self.isFavorite = true
    }
    
    func removeFavoriteMovie() {
        self.favouriteButton.setImage(UIImage(named: "outlined_star"), for: .normal)
        self.isFavorite = false
    }
    
    func showSaveFavoriteError(error: String) {
        let alert = AlertUtils.createOneButtonAlertController(title: "Erro", message: error, buttonTitle: "OK", withHandler: nil)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showRemoveFavoriteError(error: String) {
        let alert = AlertUtils.createOneButtonAlertController(title: "Erro", message: error, buttonTitle: "OK", withHandler: nil)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showGetMovieError(error: String) {
        let alert = AlertUtils.createOneButtonAlertController(title: "Erro", message: error, buttonTitle: "OK") { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func addDetailedMovie(detailedMovie: DetailedMovie, isFavorite: Bool) {
        self.setUpView(detailedMovie: detailedMovie, isFavorite: isFavorite)
    }
}
