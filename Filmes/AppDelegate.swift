//
//  AppDelegate.swift
//  Filmes
//
//  Created by João Gabriel on 10/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import UIKit
import Swinject
import Moya

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: MainCoordinator?
    
    private let container: Container = {
       let container = Container()
        container.register(MoyaProvider<MyServerAPI>.self, factory: { resolver in
            return MoyaProvider<MyServerAPI>(plugins: [CachePolicyPlugin()])
        })
        
        container.register(CacheDataSource.self, factory: { resolver in
            return CacheDataSource()
        }).inObjectScope(.container)
        
        container.register(MainViewController.self, factory: { resolver in
            return MainViewController.instantiate(storyboard: "Main")
        }).initCompleted({ (resolver: Resolver, viewController: MainViewController) -> Void in
            viewController.presenter = container.resolve(MoviePresenter.self)
        }).inObjectScope(.container)
        
        container.register(DetailedViewController.self, factory: { resolver in
            return DetailedViewController.instantiate(storyboard: "MovieDetail")
        }).initCompleted({ (resolver: Resolver, viewController: DetailedViewController) -> Void in
            viewController.presenter = container.resolve(DetailedMoviePresenter.self)
        }).inObjectScope(.container)
        
        container.register(MainRepository.self, factory: { (resolver: Resolver) -> MainRepository in
            return MainRepository(provider: container.resolve(MoyaProvider<MyServerAPI>.self)!, cacheSource: container.resolve(CacheDataSource.self)!)
        }).inObjectScope(.container)
        
        let viewController: UIViewController = MainViewController.instantiate(storyboard: "Main")
        container.register(MoviePresenter.self, factory: { resolver in
            return MoviePresenter(repository: container.resolve(MainRepository.self)!, view: container.resolve(MainViewController.self)!)
        }).inObjectScope(.container)
        
        container.register(DetailedMoviePresenter.self, factory: { resolver in
            return DetailedMoviePresenter(repository: container.resolve(MainRepository.self)!, view: container.resolve(DetailedViewController.self)!)
        }).inObjectScope(.container)
        
       return container
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let navController = UINavigationController()
        coordinator = MainCoordinator(navigationController: navController, container: container)
        coordinator?.start()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

