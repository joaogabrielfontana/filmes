//
//  MainCoordinator.swift
//  Filmes
//
//  Created by João Gabriel on 17/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import Foundation
import UIKit
import Swinject

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    private let navigationController: UINavigationController
    private let container: Container
    
    init(navigationController: UINavigationController, container: Container) {
        self.navigationController = navigationController
        self.container = container
    }
    
    func start() {
        let vc = container.resolve(MainViewController.self)!
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func navigateToMovieDetail(id: Int) {
        let vc = container.resolve(DetailedViewController.self)!
        vc.coordinator = self
        vc.id = id
        navigationController.pushViewController(vc, animated: true)
    }
}
