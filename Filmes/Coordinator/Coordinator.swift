//
//  Coordinator.swift
//  Filmes
//
//  Created by João Gabriel on 17/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    func start()
}
