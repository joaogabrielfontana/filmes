//
//  DetailedMovie.swift
//  Filmes
//
//  Created by João Gabriel on 10/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

struct DetailedMovie: Codable, DefaultsSerializable {
    //MARK: Properties
    let id: Int
    let title: String
    let posterURL: String
    let budget: Int64
    let revenue: Int64
    let overview: String
    let runtime: Int
    let voteAverage: Double
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case posterURL = "poster_url"
        case budget = "budget"
        case revenue = "revenue"
        case overview = "overview"
        case runtime = "runtime"
        case voteAverage = "vote_average"
    }
}
