//
//  Movie.swift
//  Filmes
//
//  Created by João Gabriel on 10/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

struct Movie: Codable, DefaultsSerializable {
    //MARK: Properties
    let id: Int
    let voteAverage: Double
    let title: String
    let posterURL: String
    let genres: [String]
    let releaseDate: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case voteAverage = "vote_average"
        case title = "title"
        case posterURL = "poster_url"
        case genres = "genres"
        case releaseDate = "release_date"
    }
}
