//
//  AlertUtils.swift
//  Filmes
//
//  Created by João Gabriel on 06/08/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import UIKit

class AlertUtils {
    static func createOneButtonAlertController(title: String? = nil,
                                               message: String? = nil,
                                               buttonTitle: String? = nil,
                                               withHandler handler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let buttonAction = UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: handler)
        
        let alert = UIAlertController(
            title: title ?? "",
            message: message ?? "",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(buttonAction)
        
        return alert
    }
}
