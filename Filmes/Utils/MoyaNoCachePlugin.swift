//
//  MoyaNoCachePlugin.swift
//  Filmes
//
//  Created by João Gabriel on 25/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import Foundation
import Moya
import Result
protocol CachePolicyGettable {
    var cachePolicy: URLRequest.CachePolicy { get }
}
//Usage: MoyaProvider<ServiceProvider>(plugins: [CachePolicyPlugin()])
final class CachePolicyPlugin: PluginType {
    public func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        if let cachePolicyGettable = target as? CachePolicyGettable {
            var mutableRequest = request
            mutableRequest.cachePolicy = cachePolicyGettable.cachePolicy
            return mutableRequest
        }
        return request
    }
}
extension MyServerAPI: CachePolicyGettable {
    var cachePolicy: URLRequest.CachePolicy {
        switch self {
        case .movies, .detailedMovies(_):
            return .reloadIgnoringLocalCacheData
        }
    }
}

