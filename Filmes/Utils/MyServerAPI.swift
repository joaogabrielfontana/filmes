//
//  MyServerAPI.swift
//  Filmes
//
//  Created by João Gabriel on 10/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import Foundation
import Moya

enum MyServerAPI {
    case movies
    case detailedMovies(movieID: Int)
}

extension MyServerAPI: TargetType {
    
    var baseURL: URL {
        guard let url = URL(string: "https://desafio-mobile.nyc3.digitaloceanspaces.com") else {
            fatalError("FAILED: https://desafio-mobile.nyc3.digitaloceanspaces.com")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .movies:
            return "/movies"
        case .detailedMovies(let movieID):
            return "/movies/\(movieID)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        default: return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
         return ["Content-Type": "application/json"]
    }
}
