//
//  MainViewController.swift
//  Filmes
//
//  Created by João Gabriel on 11/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import RxCocoa

class MainViewController: UIViewController, Storyboarded {
    //MARK: IBOutlets
    @IBOutlet weak var moviesTableView: UITableView! {
        didSet {
            self.moviesTableView.register(MovieTableViewCell.self, forCellReuseIdentifier: String(describing: MovieTableViewCell.self))
            self.moviesTableView.register(UINib(nibName: String(describing: MovieTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: MovieTableViewCell.self))
            self.moviesTableView.delegate = self
        }
    }
    
    //MARK: Variables
    var movies: BehaviorRelay<[Movie]> = BehaviorRelay(value: [])
    
    var presenter: MoviePresenter!
    
    var coordinator: MainCoordinator?
    
    let disposeBag = DisposeBag()
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getMovies()
        
        self.movies.bind(to: self.moviesTableView.rx.items(cellIdentifier: String(describing: MovieTableViewCell.self), cellType: MovieTableViewCell.self)) { (row, model, cell) in
            cell.setUpCell(movie: model)
        }.disposed(by: disposeBag)
        
        self.navigationItem.title = "Filmes"
        self.navigationController?.navigationBar.backgroundColor = UIColor.yellow
    }
    
    func getMovies() {
        self.presenter.getMovies()
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.coordinator?.navigateToMovieDetail(id: movies.value[indexPath.row].id)
    }
}

extension MainViewController: MoviesListView {
    func showGetMovieError(error: String) {
        let alert = UIAlertController(
            title: "Erro",
            message: error,
            preferredStyle: UIAlertController.Style.alert
        )
        self.present(alert, animated: true, completion: nil)
    }
    
    func addMovies(movies: [Movie]) {
        self.movies.accept(movies)
    }
}
