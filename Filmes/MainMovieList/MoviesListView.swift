//
//  MoviesListView.swift
//  Filmes
//
//  Created by João Gabriel on 15/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import Foundation

protocol MoviesListView {
    func addMovies(movies: [Movie])
    func showGetMovieError(error: String)
}
