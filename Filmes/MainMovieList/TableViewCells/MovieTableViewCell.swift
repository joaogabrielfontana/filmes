//
//  MovieTableViewCell.swift
//  Filmes
//
//  Created by João Gabriel on 11/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var moviePosterImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieGenresLabel: UILabel!
    @IBOutlet weak var movieReleaseDateLabel: UILabel!
    @IBOutlet weak var voteAverageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func setUpCell(movie: Movie) {
        self.contentView.backgroundColor = UIColor.yellow
        let placeholder = UIImage(named: "placeholder")
        self.movieTitleLabel.text = movie.title
        self.movieGenresLabel.text = movie.genres.joined(separator: ", ")
        self.movieReleaseDateLabel.text = movie.releaseDate
        self.voteAverageLabel.text = String(movie.voteAverage)
        self.moviePosterImageView.kf.indicatorType = .activity
        self.moviePosterImageView.kf.setImage(with: URL(string: movie.posterURL), placeholder: placeholder)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
