//
//  MoviePresenter.swift
//  Filmes
//
//  Created by João Gabriel on 15/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import Foundation
import Moya
import RxMoya
import RxSwift

class MoviePresenter {
    
    private var moviesView: MoviesListView?
    
    private let repository: MainRepository
    
    private let disposeBag = DisposeBag()
    
    init(repository: MainRepository, view: MoviesListView) {
        self.moviesView = view
        self.repository = repository
    }
    
    
    //Internet then Cache
    func getMovies() {
       self.repository.getMovies()
        .subscribe(onSuccess: { [weak self] movies in
            self?.moviesView?.addMovies(movies: movies)
            }, onError: { [weak self] error in
                self?.moviesView?.showGetMovieError(error: error.localizedDescription)
        }).disposed(by: self.disposeBag)
    }
    
    //Cache then Internet
    func getMoviesFromCache() {
        self.repository.getMoviesFromCache()
            .subscribe(onSuccess: { [weak self] movies in
                self?.moviesView?.addMovies(movies: movies)
                }, onError: { [weak self] error in
                    self?.moviesView?.showGetMovieError(error: error.localizedDescription)
            }).disposed(by: self.disposeBag)
    }
}
