//
//  MainRepository.swift
//  Filmes
//
//  Created by João Gabriel on 05/08/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import RxMoya

class MainRepository {
    
    private let provider: MoyaProvider<MyServerAPI>
    private let cacheSource: CacheDataSource
    
    init (provider: MoyaProvider<MyServerAPI>, cacheSource: CacheDataSource) {
        self.provider = provider
        self.cacheSource = cacheSource
    }

    func getMovies() -> Single<[Movie]> {
        return provider.rx.request(.movies).map([Movie].self)
            .flatMap { movies in
                return self.cacheSource.storeMovieList(movieList: movies).catchError { _ in Completable.empty() }.andThen(Single.just(movies))
            }
            .catchError {
                error in
                return self.cacheSource.getMovieList().flatMap { cacheList in
                    guard let cacheList = cacheList else {
                        return Single.error(error)
                    }
                    return Single.just(cacheList)
                }
        }
    }
    
    func getDetailedMovie(movieID: Int) -> Single<DetailedMovie> {
        return provider.rx.request(.detailedMovies(movieID: movieID))
            .map(DetailedMovie.self)
            .flatMap { detailedMovie in
                return self.cacheSource.insertDetailedMovieOnCache(detailedMovie: detailedMovie).andThen(Single.just(detailedMovie))
            }
            .catchError { error in
                return self.cacheSource.getDetailedMovieList()
                    .flatMap { detailedMovieList in
                        guard let detailedMovie  = detailedMovieList?.first(where: { (detailedMovie) -> Bool in
                            return detailedMovie.id == movieID
                        }) else {
                            return Single.error(error)
                        }
                        return Single.just(detailedMovie)
                }
        }
    }

    
    func isFavoriteMovie(movieId: Int) -> Single<Bool>{
        return cacheSource.isMovieFavorite(movieId: movieId)
    }
    
    func saveFavoriteMovie(movieId: Int) -> Completable {
        return self.cacheSource.saveMovieAsFavorite(movieId: movieId)
    }
    
    func removeFavoriteMovie(movieId: Int) -> Completable {
        return self.cacheSource.removeMovieAsFavorite(movieId: movieId)
    }
    
    func getDetailedMovieFromCache(movieID: Int) -> Single<DetailedMovie> {
        return self.cacheSource.getDetailedMovieList()
            .flatMap { detailedMoviesList in
                guard let detailedMovie = detailedMoviesList?.first(where: { (storedDetailedMovie) -> Bool in
                    storedDetailedMovie.id == movieID
                }) else {
                    return self.provider.rx.request(.detailedMovies(movieID: movieID)).map(DetailedMovie.self)
                        .flatMap { movie in
                            return self.cacheSource.insertDetailedMovieOnCache(detailedMovie: movie).andThen(Single.just(movie))
                            
                    }
                }
                return Single.just(detailedMovie)
        }
    }
    
    func getMoviesFromCache() -> Single<[Movie]> {
        return cacheSource.getMovieList().flatMap { moviesList in
            guard let moviesList = moviesList else {
                return self.provider.rx.request(.movies).map([Movie].self)
            }
            return Single.just(moviesList)
        }
    }
}
