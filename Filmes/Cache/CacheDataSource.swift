//
//  CacheDataSource.swift
//  Filmes
//
//  Created by João Gabriel on 25/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import SwiftyUserDefaults
import RxSwift

extension DefaultsKeys {
    static let movieList = DefaultsKey<[Movie]?>("movieList")
    static let detailedMovie = DefaultsKey<DetailedMovie?>("detailedMovie")
    static let detailedMovieList = DefaultsKey<[DetailedMovie]?>("detailedMovieList")
    static let favoriteMoviesIds = DefaultsKey<[Int]?>("favoriteMoviesIds")
}

class CacheDataSource {
    func storeMovieList(movieList: [Movie]) -> Completable {
        return Completable.create { completable in
            Defaults[.movieList] = movieList
            completable(.completed)
            return Disposables.create()
        }
    }
    
    func storeDetailedMovieList(detailedMovieList: [DetailedMovie]) -> Completable {
        return Completable.create { completable in
            Defaults[.detailedMovieList] = detailedMovieList
            completable(.completed)
            return Disposables.create()
        }
    }
    
    func storeFavoriteMoviesIds(moviesIds: [Int]) -> Completable {
        return Completable.create { completable in
            Defaults[.favoriteMoviesIds] = moviesIds
            completable(.completed)
            return Disposables.create()
        }
    }
    
    func insertDetailedMovieOnCache(detailedMovie: DetailedMovie) -> Completable {
        return self.getDetailedMovieList()
            .flatMapCompletable { moviesList in
                guard let detailedMovieList = moviesList else {
                    var list = [DetailedMovie]()
                    list.append(detailedMovie)
                    return self.storeDetailedMovieList(detailedMovieList: list)
                }
                var list = detailedMovieList
                list.append(detailedMovie)
                return self.storeDetailedMovieList(detailedMovieList: list)
        }
    }
    
    func saveMovieAsFavorite(movieId: Int) -> Completable {
        return self.getFavoriteMoviesIds()
            .flatMapCompletable { moviesIds in
                guard let unwrappedIds = moviesIds else {
                    var moviesIds = [Int]()
                    moviesIds.append(movieId)
                    return self.storeFavoriteMoviesIds(moviesIds: moviesIds)
                }
                var favoriteIds = unwrappedIds
                favoriteIds.append(movieId)
                return self.storeFavoriteMoviesIds(moviesIds: favoriteIds)
        }
    }
    
    func removeMovieAsFavorite(movieId: Int) -> Completable {
        return self.getFavoriteMoviesIds()
            .flatMapCompletable { moviesIds in
                guard let unwrappedIds = moviesIds else {
                    return self.storeFavoriteMoviesIds(moviesIds: [])
                }
                var favoriteIds = unwrappedIds
                favoriteIds.removeAll(where: { (id) -> Bool in
                    id == movieId
                })
                return self.storeFavoriteMoviesIds(moviesIds: favoriteIds)
        }
    }
    
    func isMovieFavorite(movieId: Int) -> Single<Bool> {
        return self.getFavoriteMoviesIds()
            .map { moviesIds in moviesIds?.contains(movieId) ?? false }
    }

    func getMovieList() -> Single<[Movie]?> {
        return Single.create { single in
            single(.success(Defaults[.movieList]))
            return Disposables.create {}
        }
    }

    func getFavoriteMoviesIds() -> Single<[Int]?> {
        return Single.create { single in
            let moviesIds = Defaults[.favoriteMoviesIds]
            single(.success(moviesIds))
            return Disposables.create {}
        }
    }
    
    func getDetailedMovieList() -> Single<[DetailedMovie]?> {
        return Single.create { single in
            single(.success(Defaults[.detailedMovieList]))
            return Disposables.create {}
        }
    }
}
